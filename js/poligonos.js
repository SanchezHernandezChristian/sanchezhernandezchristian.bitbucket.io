var poligonos = [
    {
        "nombre":"Edificio A",
        "coordenadas":[-96.7457831,17.0785712,-96.7458301,17.078452,-96.7454854,17.0783405,-96.7454425,17.0784559],
        "informacion":

    '<div class="list-group">'+

        '<a class="list-group-item list-group-item-action active">'+
        "Número de Aulas:"+
         "</a>"+

         '<a class="list-group-item list-group-item-action">8</a>'+

         '<a class="list-group-item list-group-item-action active">'+
        "Planta Alta:"+
         "</a>"+

        '<a class="list-group-item list-group-item-action">Aulas A5 -A8</a>'+


        '<a class="list-group-item list-group-item-action active">'+
        "Planta Baja:"+
         "</a>"+

         '<a class="list-group-item list-group-item-action disabled">Aulas A1 - A4</a>'+
    '</div>'+
    "<div> <img src='../fotos_ito/edificio_a.JPG' width='250' height='230'></img><div>"
    },
    {
        "nombre":"Edificio B",
        "coordenadas":[-96.7449704,17.0784206,-96.7450106,17.078336,-96.7446378,17.078195,-96.7446029,17.0782719],
        "informacion":

    '<div class="list-group">'+

        '<a class="list-group-item list-group-item-action active">'+
        "Número de Aulas:"+
         "</a>"+

         '<a class="list-group-item list-group-item-action">9</a>'+

         '<a class="list-group-item list-group-item-action active">'+
        "Planta Alta:"+
         "</a>"+

        '<a class="list-group-item list-group-item-action">Aulas B6 -B9</a>'+

        '<a class="list-group-item list-group-item-action active">'+
        "Planta Baja:"+
         "</a>"+

         '<a class="list-group-item list-group-item-action disabled">Aulas B1 - B5</a>'+

    '</div>'+
    "<div> <img src='../fotos_ito/edificio_b.JPG' width='250' height='230'></img><div>"
    },
    {
        "nombre":"Edificio C",
        "coordenadas":[-96.7449707,17.0781299,-96.7450056,17.0780427,-96.7447293,17.0779504,-96.7446918,17.0780299],
        "informacion":

    '<div class="list-group">'+

        '<a class="list-group-item list-group-item-action active">'+
        "Número de Aulas:"+
         "</a>"+

         '<a class="list-group-item list-group-item-action">9</a>'+

         '<a class="list-group-item list-group-item-action active">'+
        "Planta Alta:"+
         "</a>"+

        '<a class="list-group-item list-group-item-action">Aulas C1 -C5</a>'+

        '<a class="list-group-item list-group-item-action active">'+
        "Planta Baja:"+
         "</a>"+

         '<a class="list-group-item list-group-item-action disabled">Aulas C6 - C9</a>'+

    '</div>'+
    "<div> <img src='../fotos_ito/edificio_c.JPG' width='250' height='230'></img><div>"
    },
    {
        "nombre":"Edificio D",
        "coordenadas":[-96.7437136,17.077291,-96.7437551,17.0771744,-96.7435164,17.077091,-96.7434749,17.0772064],
        "informacion":

    '<div class="list-group">'+

        '<a class="list-group-item list-group-item-action active">'+
        "Número de Aulas:"+
         "</a>"+

         '<a class="list-group-item list-group-item-action">4</a>'+


        '<a class="list-group-item list-group-item-action">Aulas D1 - D4</a>'+
    '</div>'+
    "<div> <img src='../fotos_ito/edificio_d.JPG' width='250' height='230'></img><div>"
    },
    {
        "nombre":"Edificio E",
        "coordenadas":[-96.7443071,17.0769742,-96.7439664,17.0768665,-96.7439249,17.0769857,-96.7442588,17.0770921],
        "informacion":

    '<div class="list-group">'+

        '<a class="list-group-item list-group-item-action active">'+
        "Número de Aulas:"+
         "</a>"+

         '<a class="list-group-item list-group-item-action">3</a>'+


        '<a class="list-group-item list-group-item-action">Aulas E1 - E3</a>'+
    '</div>'+
    "<div> <img src='../fotos_ito/edificio_e.JPG' width='250' height='230'></img><div>"
    },
    {
        "nombre":"Edificio F",
        "coordenadas":[-96.7443087,17.0766788,-96.743964,17.0765711,-96.7439171,17.0767122,-96.7442591,17.076816],
        "informacion":

    '<div class="list-group">'+

        '<a class="list-group-item list-group-item-action active">'+
        "Número de Aulas:"+
         "</a>"+

         '<a class="list-group-item list-group-item-action">8</a>'+

         '<a class="list-group-item list-group-item-action active">'+
        "Planta Alta:"+
         "</a>"+

        '<a class="list-group-item list-group-item-action">Aulas F6 -F8</a>'+

        '<a class="list-group-item list-group-item-action active">'+
        "Planta Baja:"+
         "</a>"+

         '<a class="list-group-item list-group-item-action disabled">Aulas F1 - F5</a>'+

    '</div>'+
    "<div> <img src='../fotos_ito/edificio_f.JPG' width='250' height='230'></img><div>"
    },
    {
        "nombre":"Edificio G",
        "coordenadas":[-96.7442857,17.0765538,-96.7443299,17.0764321,-96.7440429,17.0763372,-96.744,17.0764564],
        "informacion":

    '<div class="list-group">'+

        '<a class="list-group-item list-group-item-action active">'+
        "Número de Aulas:"+
         "</a>"+

         '<a class="list-group-item list-group-item-action">4</a>'+

     

        '<a class="list-group-item list-group-item-action">Aulas G1 - G4</a>'+

    '</div>'+
    "<div> <img src='../fotos_ito/edificio_g.JPG' width='250' height='230'></img><div>"
    },
    {
        "nombre":"Edificio H",
        "coordenadas":[-96.7448384,17.0766981,-96.7444321,17.0765788,-96.7443932,17.0766916,-96.7448009,17.076807],
        "informacion":

    '<div class="list-group">'+
    
    '<a class="list-group-item list-group-item-action">GRIS</a>'+
    '<a class="list-group-item list-group-item-action">H. CEGRITO</a>'+

        '<a class="list-group-item list-group-item-action active">'+
        "Número de Aulas:"+
         "</a>"+

         '<a class="list-group-item list-group-item-action">2</a>'+

     

        '<a class="list-group-item list-group-item-action">Aulas H1 - H2</a>'+

    '</div>'+
    "<div> <img src='../fotos_ito/edif_H.JPG' width='250' height='230'></img><div>"
    },
    {
        "nombre":"Edificio I",
        "coordenadas":[-96.7449489,17.07619,-96.744741,17.0758413,-96.7445533,17.0759439,-96.7447544,17.0762836],
        "informacion":

    '<div class="list-group">'+

        '<a class="list-group-item list-group-item-action active">'+
        "Número de Aulas:"+
         "</a>"+

         '<a class="list-group-item list-group-item-action">14</a>'+

         '<a class="list-group-item list-group-item-action active">'+
        "Planta Alta:"+
         "</a>"+

        '<a class="list-group-item list-group-item-action">Aulas I11 - I14</a>'+
        '<a class="list-group-item list-group-item-action">Depto. de Ing. en Sistemas Computacionales</a>'+
        '<a class="list-group-item list-group-item-action disabled">Dra. Marisol Altamirano Cabrera</a>'+
        '<a class="list-group-item list-group-item-action disabled">Sanitarios</a>'+

        '<a class="list-group-item list-group-item-action active">'+
        "Planta Baja:"+
         "</a>"+

         '<a class="list-group-item list-group-item-action disabled">Aulas I1 - I10</a>'+
         '<a class="list-group-item list-group-item-action disabled">Sanitarios</a>'+
    '</div>'+
    "<div> <img src='../fotos_ito/edificio_i.JPG' width='250' height='230'></img><div>"


    },
    {
        "nombre":"Edificio J",
        "coordenadas":[-96.7446424,17.0763487,-96.7446773,17.0762423,-96.7443702,17.076141,-96.7443313,17.0762526],
        "informacion":

    "<p>Depto. de Ciencias Economico Administrativas</p>"+
    "<div> <img src='../fotos_ito/edificio_j.JPG' width='250' height='230'></img><div>"
    },
    {

    "nombre":"Edificio K",
    "coordenadas":[-96.7438525,17.076318,-96.7437399,17.0762782,-96.7436406,17.0765474,-96.7436768,17.0765603,-96.7436513,17.0766333,-96.7437264,17.0766564],
    "informacion":

    '<div class="list-group">'+

        '<a class="list-group-item list-group-item-action active">'+
        "Número de Aulas:"+
         "</a>"+

         '<a class="list-group-item list-group-item-action">1</a>'+

     

        '<a class="list-group-item list-group-item-action">Aula K1</a>'+

    '</div>'+
    "<div> <img src='../fotos_ito/edificio_k.JPG' width='250' height='230'></img><div>"
    },
    {
        "nombre":"Edificio L",
        "coordenadas":[-96.7442063,17.078106,-96.7440025,17.0780842,-96.7439958,17.0781572,-96.7441942,17.0781765],
        "informacion":
        '<div class="list-group">'+

        '<a class="list-group-item list-group-item-action active">'+
        "Número de Aulas:"+
         "</a>"+

         '<a class="list-group-item list-group-item-action">2</a>'+

     

        '<a class="list-group-item list-group-item-action">Aulas L1 - L2</a>'+

    '</div>'    
    },
    {
        "nombre":"Edificio M",
        "coordenadas":[-96.7441314,17.0786596,-96.744169,17.0785417,-96.7440912,17.0785212,-96.7440456,17.0786365],
        "informacion":

    '<div class="list-group">'+

        '<a class="list-group-item list-group-item-action active">'+
        "Número de Aulas:"+
         "</a>"+

         '<a class="list-group-item list-group-item-action">1</a>'+

     

        '<a class="list-group-item list-group-item-action">Aula M1</a>'+

    '</div>'
    },
    {
        "nombre":"Edificio N",
        "coordenadas":[-96.7440992,17.0787724,-96.7441314,17.0786596,-96.7440456,17.0786365,-96.744008,17.0787365],
        "informacion":

    '<div class="list-group">'+

        '<a class="list-group-item list-group-item-action active">'+
        "Número de Aulas:"+
         "</a>"+

         '<a class="list-group-item list-group-item-action">1</a>'+

     

        '<a class="list-group-item list-group-item-action">Aula N1</a>'+

    '</div>'
    },
    {
        "nombre":"Edificio Ñ",
        "coordenadas":[-96.7437069,17.0793421,-96.7437324,17.0792767,-96.7436613,17.0792537,-96.7436372,17.0793165],
        "informacion":

    '<div class="list-group">'+

        '<a class="list-group-item list-group-item-action active">'+
        "Número de Aulas:"+
         "</a>"+

         '<a class="list-group-item list-group-item-action">1</a>'+

     

        '<a class="list-group-item list-group-item-action">Aula Ñ1</a>'+

    '</div>'+
    "<div> <img src='../fotos_ito/edif_ñ.JPG' width='250' height='230'></img><div>"
    },
    {
    "nombre":"Edificio O",
    "coordenadas":[-96.7434274,17.0786586,-96.7434193,17.0784804,-96.7434032,17.0784804,-96.7434019,17.0783868,-96.7432638,17.0783907,-96.7432691,17.078483,-96.7433295,17.0784855,-96.7433362,17.0786586],
    "informacion":

    '<div class="list-group">'+

        '<a class="list-group-item list-group-item-action active">'+
        "Número de Aulas:"+
         "</a>"+

         '<a class="list-group-item list-group-item-action">2</a>'+

     

        '<a class="list-group-item list-group-item-action">Aula O1 - O2</a>'+

    '</div>'+
    "<div> <img src='../fotos_ito/edificio_o.JPG' width='250' height='230'></img><div>"
    },
    {
        "nombre":"Edificio P",
        "coordenadas":[-96.7433415,17.0781571,-96.7434166,17.0779314,-96.7432771,17.077893,-96.7432436,17.0779866,-96.7433026,17.0780071,-96.7432623,17.078134],
        "informacion":

    '<div class="list-group">'+

        '<a class="list-group-item list-group-item-action active">'+
        "Número de Aulas:"+
         "</a>"+

         '<a class="list-group-item list-group-item-action">2</a>'+

     

        '<a class="list-group-item list-group-item-action">Aula P1 - P2</a>'+

    '</div>'+
    "<div> <img src='../fotos_ito/edificio_p.JPG' width='250' height='230'></img><div>"
    },
    {
        "nombre":"Administración",
        "coordenadas":[-96.7452513,17.0775496,-96.745317,17.0773701,-96.7450246,17.0772675,-96.7449616,17.0774509],
        "informacion":

    '<div class="list-group">'+

        '<a class="list-group-item list-group-item-action active">'+
        "Planta Alta:"+
         "</a>"+

     

        '<a class="list-group-item list-group-item-action">Dirección</a>'+
        '<a class="list-group-item list-group-item-action">Subdirecciones Académicas</a>'+
        '<a class="list-group-item list-group-item-action">Planeación y Vinculación</a>'+
        '<a class="list-group-item list-group-item-action">Depto. de Recursos Financieros</a>'+


        '<a class="list-group-item list-group-item-action active">'+
        "Planta Baja:"+
         "</a>"+
        '<a class="list-group-item list-group-item-action">Depto. de Planeación Prog. y Presupuesto</a>'+
        '<a class="list-group-item list-group-item-action">Depto. de Comunicación y Difusión</a>'+
        '<a class="list-group-item list-group-item-action">Depto. de Gestion Tecnológica y Vinculación y Depto. de Recursos Humanos</a>'+
        '<a class="list-group-item list-group-item-action">Depto. de Recursos Financieros</a>'+
        

    '</div>'+
    "<div> <img src='../fotos_ito/Edif_adm.JPG' width='250' height='230'></img><div>"
    },
    {
        "nombre":"Depto. de Estudios Profesionales y Depto. de Servicios Escolares",
        "coordenadas":[-96.7451614,17.0777996,-96.7452285,17.0776188,-96.7449267,17.0775214,-96.7448664,17.0776944],
        "informacion":""+
        "<div> <img src='../fotos_ito/Edif_adm.JPG' width='250' height='230'></img><div>"
    },
    {
        "nombre":"Departamento de Desarrollo Academico",
        "coordenadas":[-96.7456758,17.0780588,-96.7457241,17.077946,-96.7454022,17.077837,-96.7453566,17.0779537],
        "informacion":

    "<p>Sala de Usos Multiples(SUM)</p>"+
    "<div> <img src='../fotos_ito/desarrollo_academico.JPG' width='250' height='230'></img><div>"
    },
    {
        "nombre":"Audivisual de Ingeniería",
        "coordenadas":[-96.7459735,17.0783626,-96.7460164,17.0782383,-96.745708,17.0781447,-96.7456678,17.0782614],
        "informacion":

    '<div class="list-group">'+

      

        '<a class="list-group-item list-group-item-action">Audiovisual de Ingeniería</a>'+
        '<a class="list-group-item list-group-item-action">Sanitarios</a>'+
    '</div>'+
    "<div> <img src='../fotos_ito/audiovisual_ingenieria.JPG' width='250' height='230'></img><div>"
    },
    {
        "nombre":"Sala de Titulación",
        "coordenadas":[-96.7452406,17.0784542,-96.7453774,17.0780709,-96.7452701,17.0780363,-96.7451306,17.0784196],
        "informacion":

    '<div class="list-group">'+

     

        '<a class="list-group-item list-group-item-action">Aula de dibujo</a>'+
        '<a class="list-group-item list-group-item-action">Coord. de Lenguas Extranjeras</a>'+
        '<a class="list-group-item list-group-item-action">Sanitarios</a>'+
    '</div>'+
    "<div> <img src='../fotos_ito/titulacion.JPG' width='250' height='230'></img><div>"
    },
    {
        "nombre":"Edificio de Educación a Distancia",
        "coordenadas":[-96.7451166,17.0789015,-96.7452453,17.0785131,-96.7450401,17.0784426,-96.7449006,17.0788374],
        "informacion": "<p>Maestría en Docencia</p>"+
        "<div> <img src='../fotos_ito/educacion_distancia.JPG' width='250' height='230'></img><div>"
    },
    {
        "nombre":"Centro de Cómputo",
        "coordenadas":[-96.7444158,17.0792713,-96.744507,17.0790239,-96.7443984,17.0789854,-96.7443823,17.0790226,-96.7443233,17.0790059,-96.744334,17.0789674,-96.7442522,17.0789431,-96.7441651,17.0791918],
        "informacion":

    '<div class="list-group">'+

     

        '<a class="list-group-item list-group-item-action">Depto. de Recursos Materiales</a>'+
        '<a class="list-group-item list-group-item-action">Laboratorio de redes</a>'+
    '</div>'+
    "<div> <img src='../fotos_ito/centro_computo.JPG' width='250' height='230'></img><div>"
    },
    {
        "nombre":"División de Estudios de Posgrado e Investigación  ",
        "coordenadas":[-96.7436105,17.0790806,-96.7436628,17.0789345,-96.7434267,17.078855,-96.7433771,17.0790024],
        "informacion":""    +
        "<div> <img src='../fotos_ito/posgrado.JPG' width='250' height='230'></img><div>"
    },
    {
        "nombre":"Depto. de Ciencias de la Tierra",
        "coordenadas":[-96.7440883,17.0784647,-96.7442519,17.0785108,-96.7442882,17.0784006,-96.7441299,17.0783506],
        "informacion":""+
        "<div> <img src='../fotos_ito/tierra.JPG' width='250' height='230'></img><div>"
    },
    {
        "nombre":"Sala de Maestros",
        "coordenadas":[-96.744625,17.0777903,-96.7446799,17.0776595,-96.7445096,17.0775992,-96.74446,17.0777326],
        "informacion":""+
        "<div> <img src='../fotos_ito/sala_de_maestros.JPG' width='250' height='230'></img><div>"
    },
    {
        "nombre":"Centro de Información",
        "coordenadas":[-96.7442683,17.0779722,-96.7443957,17.0775773,-96.744102,17.0774876,-96.7439733,17.0778786],
        "informacion":""+
        "<div> <img src='../fotos_ito/centro_informacion.JPG' width='250' height='230'></img><div>"
    },
    {
        "nombre":"Gimnasio",
        "coordenadas":[-96.7437525,17.0779141,-96.7438812,17.0775115,-96.7435701,17.0774282,-96.7434453,17.0778243],
        "informacion":""+
        "<div> <img src='../fotos_ito/gimnasio.JPG' width='250' height='230'></img><div>"
    },
    {
        "nombre":"Cafetería",
        "coordenadas":[-96.7441414,17.0772119,-96.7439831,17.0771593,-96.7439724,17.0771876,-96.7439483,17.0771786,-96.7438866,17.0773593,-96.7440716,17.0774119],
        "informacion":""+
        "<div> <img src='../fotos_ito/cafeteria.JPG' width='250' height='230'></img><div>"
    },
    {
        "nombre":"Laboratorio de Ing. Industrial",
        "coordenadas":[-96.7436779,17.0767382,-96.7433239,17.0766216,-96.7432555,17.0768228,-96.7436055,17.0769331],
        "informacion":

    '<div class="list-group">'+

        '<a class="list-group-item list-group-item-action">Almacén</a>'+
     
    '</div>'+
    "<div> <img src='../fotos_ito/laboratorio_industrial.JPG' width='250' height='230'></img><div>"
    },
    {
        "nombre":"Sala de Titulacion de Maestría",
        "coordenadas":[-96.7442858,17.0761413,-96.7441329,17.0760811,-96.744094,17.076167,-96.7442469,17.0762272],
        "informacion": "" +
        "<div> <img src='../fotos_ito/titulacion_maestria.JPG' width='250' height='230'></img><div>"
    },
    {
        "nombre":"Audiovisual de Licenciatura",
        "coordenadas":[-96.7447846,17.0765872,-96.7448382,17.0764154,-96.7445217,17.0763192,-96.7444641,17.076491],
        "informacion":

    '<div class="list-group">'+
        '<a class="list-group-item list-group-item-action">Delegación Sindical</a>'+
     
    '</div>'+
    "<div> <img src='../fotos_ito/delegacion_sindical.JPG' width='250' height='230'></img><div>"
    },
    {
        "nombre":"Departamento de Ciencias Básicas",
        "coordenadas":[-96.7446598,17.0770178,-96.7446933,17.0769229,-96.7444144,17.0768267,-96.7443741,17.0769203],
        "informacion":

    '<div class="list-group">'+
        '<a class="list-group-item list-group-item-action">Salas de Maestros A y B</a>'+
      
     
    '</div>'+
    "<div> <img src='../fotos_ito/ciencias_basicas.JPG' width='250' height='230'></img><div>"
    },
    {
        "nombre":"Explanada Principal",
        "coordenadas":[-96.7448915,17.0774505,-96.7450015,17.0771659,-96.7444838,17.0770224,-96.74439,17.0773044],
        "informacion":""+
        "<div> <img src='../fotos_ito/explanada.JPG' width='250' height='230'></img><div>"
    },
    {
        "nombre":"Asta Bandera",
        "coordenadas":[-96.744817,17.0774869,-96.7445675,17.0774074,-96.7445434,17.0774843,-96.7447821,17.0775638],
        "informacion":""+
        "<div> <img src='../fotos_ito/Asta.JPG' width='250' height='230'></img><div>"
    },
    {
        "nombre":"Laboratorio de Química 2",
        "coordenadas":[-96.744442,17.0783232,-96.7445841,17.0779334,-96.7444688,17.0779001,-96.744332,17.0782821],
        "informacion":""+
        "<div> <img src='../fotos_ito/lab_quim.JPG' width='250' height='230'></img><div>"
    },
    {
        "nombre":"Laboratorio de Química 1",
        "coordenadas":[-96.744729,17.0787283,-96.744788,17.0785744,-96.7445519,17.0785001,-96.744501,17.0786565],
        "informacion":""+
        "<div> <img src='../fotos_ito/laboratorio_quimica_1.JPG' width='250' height='230'></img><div>"
    },
    {
        "nombre":"Laboratorio de Ingeniería Mecánica",
        "coordenadas":[-96.744729,17.0787283,-96.744501,17.0786565,-96.7444473,17.0787924,-96.744678,17.0788693],
        "informacion":""+
        "<div> <img src='../fotos_ito/laboratorio_mecanica.JPG' width='250' height='230'></img><div>"
    },
    
    
    {
        "nombre":"Departamento de Mantenimiento de Equipo",
        "coordenadas":[-96.7454566,17.0789124,-96.7455223,17.0787176,-96.7452635,17.0786407,-96.7451991,17.078833],
        "informacion": ""+
        "<div> <img src='../fotos_ito/depto_mantenimiento.JPG' width='250' height='230'></img><div>"
        
    },
    
    
    {
        "nombre":"Laboratorio de Ing. Eléctrica",
        "coordenadas":[-96.7450629,17.0791695,-96.7451353,17.0789643,-96.7448778,17.0788836,-96.7448054,17.0790861],
        "informacion":"<p>Departamento de Ingeniería Eléctrica</p>"+
        "<div> <img src='../fotos_ito/laboratorio_electrica.JPG' width='250' height='230'></img><div>"
    },
    {
        "nombre":"Laboratorio de Ingeniería Electrónica",
        "coordenadas":[-96.7438353,17.0788507,-96.7439144,17.0786135,-96.7437065,17.0785507,-96.7436261,17.0787815],
        "informacion":"<p>Departamento de Ingeniería Electrónica</p>"+
        "<div> <img src='../fotos_ito/laboratorio_electronica.JPG' width='250' height='230'></img><div>"
    },
    
    {
        "nombre":"Departamento de Química",
        "coordenadas":[-96.7440145,17.07957,-96.7440803,17.0793662,-96.7438201,17.0792842,-96.7437477,17.0794841],
        "informacion":""+
        "<div> <img src='../fotos_ito/dpto_Quimica.JPG' width='250' height='230'></img><div>"
    },
    
    {
        "nombre":"Laboratorio de Ingeniería Civil",
        "coordenadas":[-96.7440992,17.0787724,-96.7442789,17.0788237,-96.7443513,17.0786083,-96.744169,17.0785417],
        "informacion":""+
        "<div> <img src='../fotos_ito/laboratorio_civil.JPG' width='250' height='215'></img><div>"
    },
    {
        "nombre":"Laboratorio de simulación de Ing. Industrial",
        "coordenadas":[-96.744888,17.0792086,-96.7447673,17.0791624,-96.7447164,17.0792881,-96.7448397,17.0793368,0],
        "informacion":""+
        "<div> <img src='../fotos_ito/lab_industrial.JPG' width='250' height='215'></img><div>"
    },
    {
        "nombre":"Cancha de futbol y atletismo",
        "coordenadas":[
            -96.7451871,17.0811374,
            -96.7451067,17.0799169,
            -96.7450155,17.0799272,
            -96.744876,17.0798503,
            -96.7446292,17.0798195,
            -96.7444254,17.0798913,
            -96.7442859,17.08004,
            -96.7442323,17.0802041,
            -96.7442859,17.0811117,
            -96.7443181,17.0811886,
            -96.7444039,17.0813014,
            -96.7444951,17.081404,
            -96.7446721,17.0814707,
            -96.7448653,17.0814399,
            -96.744994,17.0813784,
            -96.7450959,17.0812399,
            -96.7451871,17.0811374],
        "informacion":""+
        "<div> <img src='../unidad_deportiva/futbol_atletismo.jpg' width='250' height='215'></img><div>"
    },
    {
        "nombre":"Estadio de futbol",
        "coordenadas":[
            -96.7471719,17.0799015,
            -96.7470968,17.0798861,
            -96.7470968,17.0798041,
            -96.7469895,17.0797938,
            -96.7468823,17.0797118,
            -96.7460293,17.0797528,
            -96.7460025,17.0798349,
            -96.7458255,17.0798554,
            -96.7458416,17.0799477,
            -96.7457879,17.0799579,
            -96.7458147,17.080922,
            -96.7458684,17.080922,
            -96.7458737,17.0810091,
            -96.7459381,17.0809989,
            -96.7460722,17.0810655,
            -96.7460883,17.0812091,
            -96.7469413,17.0811835,
            -96.7469198,17.0810707,
            -96.7470378,17.080963,
            -96.7471558,17.0809476,
            -96.7471558,17.0808758,
            -96.7472256,17.0808656,
            -96.7471719,17.0799015
        ],
        "informacion":""+
        "<div> <img src='../unidad_deportiva/estadio.jpg' width='250' height='215'></img><div>"
    },
    {
        "nombre":"Estadio de beisbol",
        "coordenadas":[
            -96.746137,17.081875,
            -96.7455254,17.0810956,
            -96.7454289,17.0810443,
            -96.7453377,17.0811161,
            -96.7447315,17.0819212,
            -96.7453752,17.0824442,
            -96.7454664,17.0824699,
            -96.7455523,17.0824596,
            -96.7456488,17.0823981,
            -96.746137,17.081875
        ],
        "informacion":""+
        "<div> <img src='../unidad_deportiva/baseball.jpg' width='250' height='215'></img><div>"
    },
    {
        "nombre":"Alberca",
        "coordenadas":[
            -96.7464991,17.0818391,
            -96.7464723,17.0813238,
            -96.7462362,17.0813366,
            -96.7462577,17.0818417,
            -96.7464991,17.0818391
        ],
        "informacion":""+
        "<div> <img src='../unidad_deportiva/alberca.jpg' width='250' height='215'></img><div>"
    },
    {
        "nombre":"Futbol rápido",
        "coordenadas":[
            -96.7467646,17.0818212,
            -96.7467405,17.0813699,
            -96.7465447,17.0813802,
            -96.7465688,17.0818263,
            -96.7467646,17.0818212
        ],
        "informacion":""+
        "<div> <img src='../unidad_deportiva/futbol_rapido.jpg' width='250' height='215'></img><div>"
    },
    {
        "nombre":"Canchas de basquetbol",
        "coordenadas":[
            -96.7469631,17.0816699,
            -96.7471321,17.0816597,
            -96.7471482,17.081593,
            -96.7473011,17.0815776,
            -96.7472957,17.0813084,
            -96.747124,17.0813238,
            -96.747124,17.0813776,
            -96.7469443,17.0813828,
            -96.7469443,17.0814776,
            -96.74677,17.0814802,
            -96.7467834,17.0817648,
            -96.7469631,17.0817597,
            -96.7469631,17.0816699
        ],
        "informacion":""+
        "<div> <img src='../unidad_deportiva/bascket.jpg' width='250' height='215'></img><div>"
    },
    {
        "nombre":"Cancha de Pelota Mixteca",
        "coordenadas":[
            -96.7456756,17.0806803,
            -96.745622,17.0797547,
            -96.7454074,17.0797701,
            -96.7454503,17.0806905,
            -96.7456756,17.0806803
        ],
        "informacion":""+
        "<div> <img src='../unidad_deportiva/pelota_mixteca.jpg' width='250' height='215'></img><div>"
    },
    {
        "nombre":"Vagón",
        "coordenadas":[
            -96.7453913,17.0805982,
            -96.7453752,17.080429,
            -96.7453377,17.0804341,
            -96.7453484,17.0805982,
            -96.7453913,17.0805982
        ],
        "informacion":""+
        "<div> <img src='../unidad_deportiva/vagon.jpg' width='250' height='215'></img><div>"
    },
    {
        "nombre":"Servicios",
        "coordenadas":[
            -96.7455765,17.080972,
            -96.7455698,17.0808759,
            -96.7453163,17.08089,
            -96.7453217,17.0809849,
            -96.7455765,17.080972
        ],
        "informacion":""+
        "<div> <img src='../unidad_deportiva/servicios.jpg' width='250' height='215'></img><div>"
    },
    {
        "nombre":"Cancha de Basquetbol",
        "coordenadas":[
            -96.7448355,17.0817561,
            -96.7448221,17.0814689,
            -96.7446477,17.0814792,
            -96.7446558,17.0817715,
            -96.7448355,17.0817561
        ],
        "informacion":""
    }
];












var punto = function(jsonpuntos){
    var puntos_poligonos = new Array();
    jsonpuntos.forEach(function(elemento){
        var puntos = new Array();
        for(var i=0;i<elemento.coordenadas.length-1;i=i+2){
            var objeto = {"lat":elemento.coordenadas[i+1],"lng":elemento.coordenadas[i]};
            //console.log(elemento.coordenadas[i+1]+" "+elemento.coordenadas[i]);
            puntos.push(objeto);
        }
        var obj = new Array();
        obj.nombre = elemento.nombre;
        obj.coordenadas = puntos;
        obj.informacion = elemento.informacion;
        puntos_poligonos.push(obj);
        
    });
    
    
    
    return puntos_poligonos;
    }